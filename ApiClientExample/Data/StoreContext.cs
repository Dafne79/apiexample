﻿using ApiClientExample.Domain.Aggregates.ProductAggregate;
using ApiClientExample.Domain.Enumerations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Data
{
    public class StoreContext : DbContext
    {
        private static bool _isCreated = false;

        /// <summary>
        /// Contexto de almacenamiento
        /// </summary>
        /// <param name="options"></param>
        public StoreContext(DbContextOptions<StoreContext> options): base(options)
        {
            
            if (!_isCreated)
            {
                _isCreated = true;
                Database.EnsureDeleted();
                Database.EnsureCreated();
            }
        }

        /// <summary>
        /// Configura la instancia cuando se llama al metodo de addDbContext
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("DataSource=Data/Store.db");
        }

        /// <summary>
        /// Lista de productos
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Catalogo de status de producto
        /// </summary>
        public DbSet<ProductStatus> ProductStatuses { get; set; }

        /// <summary>
        /// Catalogo de categoria de productos
        /// </summary>
        public DbSet<ProductCategory> ProductCategories { get; set; }

        /// <summary>
        /// Define el proceso y las relaciones de la creacion de las tablas
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Definimos la composicion de la tabla de catalogo product status
            builder.Entity<ProductStatus>( ps =>
            {
                ps.ToTable(nameof(ProductStatus));
                ps.HasKey(x => x.Id);
                ps.HasData(ProductStatus.GetAll());
                //ps.HasMany(x => x.Products).WithOne();
            });

            // Definimos la composicion de la tabla de catalogo product category
            builder.Entity<ProductCategory>(pc =>
            {
                pc.ToTable(nameof(ProductCategory));
                pc.HasKey(x => x.Id);
                pc.HasData(ProductCategory.GetAll());
                ///pc.HasMany(x => x.Products).WithOne();
            });

            // Definimmos la commposicion de la tabla de producto
            builder.Entity<Product>(p =>
            {
                p.ToTable(nameof(Product));
                p.HasKey(x => x.Id);
                p.Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
                p.HasOne(x => x.Status).WithMany();
                p.HasOne(x => x.Category).WithMany();
            });
        }
    }
}
