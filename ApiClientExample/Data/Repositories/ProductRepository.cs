﻿using ApiClientExample.Domain.Aggregates.ProductAggregate;
using ApiClientExample.Domain.Enumerations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        /// <summary>
        /// Contiene el acceso a los datos desde EF como unidad de trabajo
        /// </summary>
        private readonly StoreContext _context;

        /// <summary>
        /// Constructor del repositorio utilizando el contexto
        /// </summary>
        /// <param name="context"></param>
        public ProductRepository(StoreContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Agrega una nueva entidad de producto
        /// </summary>
        /// <param name="product"></param>
        public void Add(Product product)
        {
            // Asociamos al seguimiento de la categoria
            _context.Attach<ProductCategory>(product.Category);
            // Agregamos el seguimiento del status
            _context.Attach<ProductStatus>(product.Status);
            // Guardamos el aggregado
            _context.Products.Add(product);
        }

        /// <summary>
        /// Elimina una entidad de producto con el id especificado
        /// </summary>
        /// <param name="id"></param>
        public void Delete(Product product)
        {
            // Lo eliminamos
            _context.Products.Remove(product);
        }

        /// <summary>
        /// Obtiene todos los productos almacenados
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Product> GetAll()
        {
            // Recuperamos las entidades de producto
            return _context
                .Products
                .Include(x => x.Category)
                .Include(x => x.Status)
                .Select(x => x);
        }

        /// <summary>
        /// Obtiene un producto en base al id ppasado por parametro
        /// </summary>
        /// <returns></returns>
        public Product GetById(int id)
        {
            // Buscamos el producto por id y lo devolvemos
            return _context
                .Products
                .Include(x => x.Category)
                .Include(x => x.Status)
                .FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Actualiza una entidad de producto
        /// </summary>
        /// <param name="product"></param>
        public void Update(Product product)
        {
            // Verificamos que la categoria siga siendo trackeada
            if (_context.Entry(product.Category).State == EntityState.Detached)
            {
                // Buscamos la entidad para intercambiarla por una trackeada
                var entityToDetach = _context.ProductCategories.Where(x => x.Id == product.Category.Id).FirstOrDefault();
                // Si existe la entidad trackeada la desasociados
                if(!(entityToDetach is null))
                    _context.Entry<ProductCategory>(entityToDetach).State = EntityState.Detached;
                // Asociamos nuestra entidad
                _context.ProductCategories.Attach(product.Category);
            }
                
            // Verificamos que el status siga siendo trackeado
            if (_context.Entry(product.Status).State == EntityState.Detached)
            {
                // Buscamos la entidad para intercambiarla por una trackeada
                var entityToDetach = _context.ProductStatuses.Where(x => x.Id == product.Status.Id).FirstOrDefault();
                // Si existe la entidad trackeada la desasociados
                if (!(entityToDetach is null))
                    _context.Entry<ProductStatus>(entityToDetach).State = EntityState.Detached;
                // Asociamos nuestra entidad
                _context.ProductStatuses.Attach(product.Status);
            }
            // Actualizamos el agregado
            _context.Products.Update(product);
        }
    }
}
