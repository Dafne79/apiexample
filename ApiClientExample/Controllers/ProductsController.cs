﻿using ApiClientExample.Comands.CreateProduct;
using ApiClientExample.Comands.DeleteProductById;
using ApiClientExample.Comands.GetAllProducts;
using ApiClientExample.Comands.GetProductById;
using ApiClientExample.Comands.UpdateProduct;
using ApiClientExample.Data;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiClientExample.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        /// <summary>
        /// Abtraccion para realizar los comandos y consultas al sistema
        /// </summary>
        private readonly IMediator _mediator;

        /// <summary>
        /// Constructor del controlador de productos
        /// </summary>
        public ProductsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Obtiene todos los productos del sistema
        /// </summary>
        /// <route>api/Products</route>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllProductsAsync()
        {
            try
            {
                // Enviamos el comando
                var products = await _mediator.Send(new GetAllProductCommand());
                // Devolvemos la respuesta
                return StatusCode(200,products);
            }
            catch (Exception ex)
            {
                // Si falla, devolvemos una respuesta de error
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Devuelve un producto basandose en su id
        /// </summary>
        /// <param name="Id"></param>
        /// <route>api/Products/{Id}</route>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            try
            {
                // Enviamos el query
                var product = await _mediator.Send<ProductFound>(new GetProductByIdCommand() { ProductId = id });
                // Devolvemos la respuesta
                return StatusCode(200, product);
            }
            catch (DataException ex)
            {
                // Si no existe, devolvemos 404
                return StatusCode(404, ex.Message);
            }
            catch (Exception ex)
            {
                // Cualquier error, devolvemos error de servidor
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Crea un producto con la informacion recibida
        /// </summary>
        /// <param name="productInformation"></param>
        /// <route>api/Products/</route>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] ProductCreationCommand productInformation)
        {
            try
            {
                // Enviammoms el comando de creacion
                var productId = await _mediator.Send<int>(productInformation);
                // Devolvemos el id creado
                return StatusCode(201, productId);
            }
            catch (Exception ex)
            {
                // Cualquier error devolvemos error de servidor
                return StatusCode(500, ex.Message);
            }
            
        }

        /// <summary>
        /// Actualiza una entidad con la informacion proporcionada en el body
        /// </summary>
        /// <param name="id"></param>
        /// <param name="productUpdateInformation"></param>
        /// <route>api/Products/{id}</route>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] UpdateProductCommand productUpdateInformation)
        {
            try
            {
                // Setteamos el id de producto a modificar
                productUpdateInformation.ProductId = id;
                // Enviamos el comando
                await _mediator.Send(productUpdateInformation);
                // Devolvemos el indicador de exito
                return StatusCode(200, "Product updated");
            }
            catch (Exception ex)
            {
                // Cualquier error, devolvemos error de servidor
                return StatusCode(500,ex.Message);
            }
        }

        /// <summary>
        /// Elimina un elemento que contenga el id especificado
        /// </summary>
        /// <param name="id"></param>
        /// <route>api/Products/{id}</route>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                // Enviamos el comando
                await _mediator.Send(new DeleteProductByIdCommand() { ProductId = id });
                // Devolvemos respuesta de exito
                return StatusCode(200, "Product removed");
            }
            catch (DataException ex)
            {
                return StatusCode(404, "Object not found");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            throw new NotImplementedException();
        }
    }
}
