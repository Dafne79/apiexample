﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Contracts
{
    /// <summary>
    /// Interface para los commandos que esperan respuesta de un tipo especifico
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public interface ICommand<out TResult> : IRequest<TResult>
    {
        Guid Id { get; }
    }

    /// <summary>
    /// Interface para los comandos que no esperan respuesta, solo ejecucion
    /// </summary>
    public interface ICommand : IRequest<Unit>
    {
        Guid Id { get; }
    }
}
