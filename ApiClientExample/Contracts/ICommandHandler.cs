﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Contracts
{
    /// <summary>
    /// Interface para los manejadores de commandos que no devuelven respuesta
    /// </summary>
    /// <typeparam name="TCommand"></typeparam>
    public interface ICommandHandler<in TCommand>
        : IRequestHandler<TCommand>
        where TCommand : ICommand
    {
    }

    /// <summary>
    /// Interface para los manejadores de comandos que devuelven respuestas de
    /// un tipo especifico
    /// </summary>
    /// <typeparam name="TCommand"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public interface ICommandHandler<in TCommand, TResult>
        : IRequestHandler<TCommand, TResult>
        where TCommand : ICommand<TResult>
    {
    }
}
