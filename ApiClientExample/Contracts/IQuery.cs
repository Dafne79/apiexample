﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Contracts
{
    /// <summary>
    /// Interface para los comandos tipo query que solo ejecutan
    /// lecturas y esperan un resultado de un tipo especifico
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public interface IQuery<out TResult> : IRequest<TResult>
    {
    }
}
