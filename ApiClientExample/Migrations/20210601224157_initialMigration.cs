﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiClientExample.Migrations
{
    public partial class initialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProductCategory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Order = table.Column<int>(type: "INTEGER", nullable: false),
                    IsVisible = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Order = table.Column<int>(type: "INTEGER", nullable: false),
                    IsVisible = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    CategoryId = table.Column<int>(type: "INTEGER", nullable: true),
                    Description = table.Column<string>(type: "TEXT", nullable: true),
                    StatusId = table.Column<int>(type: "INTEGER", nullable: true),
                    Stock = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_ProductCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ProductCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Product_ProductStatus_StatusId",
                        column: x => x.StatusId,
                        principalTable: "ProductStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "ProductCategory",
                columns: new[] { "Id", "IsVisible", "Name", "Order" },
                values: new object[] { 1, true, "Oficina", 1 });

            migrationBuilder.InsertData(
                table: "ProductCategory",
                columns: new[] { "Id", "IsVisible", "Name", "Order" },
                values: new object[] { 2, true, "Casa", 1 });

            migrationBuilder.InsertData(
                table: "ProductCategory",
                columns: new[] { "Id", "IsVisible", "Name", "Order" },
                values: new object[] { 3, true, "Construccion", 1 });

            migrationBuilder.InsertData(
                table: "ProductCategory",
                columns: new[] { "Id", "IsVisible", "Name", "Order" },
                values: new object[] { 4, true, "Alimentos", 1 });

            migrationBuilder.InsertData(
                table: "ProductCategory",
                columns: new[] { "Id", "IsVisible", "Name", "Order" },
                values: new object[] { 5, true, "Deportes", 1 });

            migrationBuilder.InsertData(
                table: "ProductStatus",
                columns: new[] { "Id", "IsVisible", "Name", "Order" },
                values: new object[] { 1, true, "Existencia disponible", 1 });

            migrationBuilder.InsertData(
                table: "ProductStatus",
                columns: new[] { "Id", "IsVisible", "Name", "Order" },
                values: new object[] { 2, true, "Fuera de existencia", 1 });

            migrationBuilder.CreateIndex(
                name: "IX_Product_CategoryId",
                table: "Product",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_StatusId",
                table: "Product",
                column: "StatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "ProductCategory");

            migrationBuilder.DropTable(
                name: "ProductStatus");
        }
    }
}
