﻿// <auto-generated />
using System;
using ApiClientExample.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ApiClientExample.Migrations
{
    [DbContext(typeof(StoreContext))]
    partial class StoreContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.6");

            modelBuilder.Entity("ApiClientExample.Domain.Aggregates.ProductAggregate.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int?>("CategoryId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Description")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<int?>("StatusId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Stock")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("StatusId");

                    b.ToTable("Product");
                });

            modelBuilder.Entity("ApiClientExample.Domain.Enumerations.ProductCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<bool>("IsVisible")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<int>("Order")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("ProductCategory");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            IsVisible = true,
                            Name = "Oficina",
                            Order = 1
                        },
                        new
                        {
                            Id = 2,
                            IsVisible = true,
                            Name = "Casa",
                            Order = 1
                        },
                        new
                        {
                            Id = 3,
                            IsVisible = true,
                            Name = "Construccion",
                            Order = 1
                        },
                        new
                        {
                            Id = 4,
                            IsVisible = true,
                            Name = "Alimentos",
                            Order = 1
                        },
                        new
                        {
                            Id = 5,
                            IsVisible = true,
                            Name = "Deportes",
                            Order = 1
                        });
                });

            modelBuilder.Entity("ApiClientExample.Domain.Enumerations.ProductStatus", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<bool>("IsVisible")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<int>("Order")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("ProductStatus");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            IsVisible = true,
                            Name = "Existencia disponible",
                            Order = 1
                        },
                        new
                        {
                            Id = 2,
                            IsVisible = true,
                            Name = "Fuera de existencia",
                            Order = 1
                        });
                });

            modelBuilder.Entity("ApiClientExample.Domain.Aggregates.ProductAggregate.Product", b =>
                {
                    b.HasOne("ApiClientExample.Domain.Enumerations.ProductCategory", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("ApiClientExample.Domain.Enumerations.ProductStatus", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId");

                    b.Navigation("Category");

                    b.Navigation("Status");
                });
#pragma warning restore 612, 618
        }
    }
}
