﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.GetAllProducts
{
    public class ProductSummary
    {
        /// <summary>
        /// Id del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Cantidad de producto en existencia
        /// </summary>
        public int Stock { get; set; }

        /// <summary>
        /// Indica el status en el que se encuentra el producto
        /// </summary>
        public string Status { get; set; }
    }
}
