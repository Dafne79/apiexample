﻿using ApiClientExample.Contracts;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.GetAllProducts
{
    /// <summary>
    /// Recupera todos los productos de la lista de objetos
    /// </summary>
    public class GetAllProductsHandler : IQueryHandler<GetAllProductCommand, List<ProductSummary>>
    {
        /// <summary>
        /// Conexion a la base de datos
        /// </summary>
        private readonly IDbConnection _dbConnection;

        /// <summary>
        /// Constructor del queryhandler que obtiene todos los productos
        /// </summary>
        /// <param name="dbConnection"></param>
        public GetAllProductsHandler(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        /// <summary>
        /// Ejecuta la busqueda de productos
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<ProductSummary>> Handle(GetAllProductCommand request, CancellationToken cancellationToken)
        {
            // Hacemos el query en sql
            var query = @$"Select 
	                        PR.Id as Id,
	                        PR.Name as Name,
	                        PRS.Name as Status,
	                        PR.Stock as Stock
                        from Product PR
                        join ProductStatus PRS on PRS.Id = PR.StatusId";
            // Leemos desde una conexion sin relacion al contexto
            var summaries = await _dbConnection.QueryAsync<ProductSummary>(query);
            // Si la lista es nula devolvemos una lista vacia
            if (summaries is null) return new List<ProductSummary>(); 
            // Devolvemos la lista recuperada en la base de datos
            return summaries.ToList();
        }
    }
}
