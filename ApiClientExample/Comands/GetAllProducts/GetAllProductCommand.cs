﻿using ApiClientExample.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.GetAllProducts
{
    public class GetAllProductCommand : IQuery<List<ProductSummary>>
    {
        /// <summary>
        /// Id del comando para cuestiones de loggeo
        /// </summary>
        public Guid Id { get; } = Guid.NewGuid();
    }
}
