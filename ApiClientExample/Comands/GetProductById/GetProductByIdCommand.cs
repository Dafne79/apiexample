﻿using ApiClientExample.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.GetProductById
{
    public class GetProductByIdCommand : IQuery<ProductFound>
    {
        /// <summary>
        /// Id del comando para cuestiones de logeo
        /// </summary>
        public Guid Id { get; } = Guid.NewGuid();

        /// <summary>
        /// Id del producto a buscar
        /// </summary>
        public int ProductId { get; set; }
    }
}
