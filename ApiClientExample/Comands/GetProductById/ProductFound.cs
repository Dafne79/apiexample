﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.GetProductById
{
    /// <summary>
    /// A simple vista es la misma entidad de producto, pero en casos mas avanzados
    /// esta clase no esta asociada a las reglas de dominio de producto, y puede
    /// agregarse la informmacion necesaria al ser un modelo aislado.
    /// </summary>
    public class ProductFound
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Id de la categoria del producto
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Nombre de la catgoria asignada
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Estado del producto
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Cantidad de producto
        /// </summary>
        public int Stock { get; set; }
    }
}
