﻿using ApiClientExample.Contracts;
using ApiClientExample.Data;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.GetProductById
{
    public class GetProductByIdHandler : IQueryHandler<GetProductByIdCommand, ProductFound>
    {
        /// <summary>
        /// Conexion con la base de datos
        /// </summary>
        private readonly IDbConnection _dbConnection;

        /// <summary>
        /// Constructor del queryhandler para ejecutar las consultas en el modelo de lectura
        /// </summary>
        /// <param name="dbConnection"></param>
        public GetProductByIdHandler(IDbConnection dbConnection)
        {
            //_context = context;
            _dbConnection = dbConnection;
        }

        /// <summary>
        /// Ejecuta el query para buscar el producto por Id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ProductFound> Handle(GetProductByIdCommand request, CancellationToken cancellationToken)
        {
            // Hacemos el query en sql
            var query = @$"Select 
	                        PR.Id as Id,
	                        PR.Name as Name,
	                        PR.Description as Description,
	                        PRC.Id as CategoryId,
	                        PRC.Name as CategoryName,
	                        PRS.Name as Status,
	                        PR.Stock as Stock
                        from Product PR
                        join ProductStatus PRS on PRS.Id = PR.StatusId
                        join ProductCategory PRC on PRC.Id = PR.CategoryId
                        where PR.Id = @Id";
            // Leemos desde una conexion sin relacion al contexto
            var productFound = await _dbConnection.QueryFirstOrDefaultAsync<ProductFound>(query, new { Id = request.ProductId });
            // Si no hay devolvemos un error
            if (productFound is null)
                throw new DataException("Product not found");
            // Devolvemos el producto encontrado
            return productFound;
        }
    }
}
