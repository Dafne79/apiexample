﻿using ApiClientExample.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.DeleteProductById
{
    /// <summary>
    /// Comando para eliminar un registro con el id especificado
    /// </summary>
    public class DeleteProductByIdCommand : ICommand
    {
        /// <summary>
        /// Id del comando.
        /// </summary>
        public Guid Id { get; } = Guid.NewGuid();

        /// <summary>
        /// Id del producto a eliminar
        /// </summary>
        public int ProductId { get; set; }
    }
}
