﻿using ApiClientExample.Contracts;
using ApiClientExample.Data;
using ApiClientExample.Domain.Aggregates.ProductAggregate;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.DeleteProductById
{
    public class DeleteProductByIdHandler : ICommandHandler<DeleteProductByIdCommand>
    {
        /// <summary>
        /// Acceso al agregado de producto
        /// </summary>
        private readonly IProductRepository _productRepository;

        /// <summary>
        /// Unidad de trabajo para confirmar los cambios
        /// </summary>
        private readonly StoreContext _context;

        /// <summary>
        /// Constructor del comando para elimminacion de producto
        /// </summary>
        /// <param name="productRepository"></param>
        /// <param name="context"></param>
        public DeleteProductByIdHandler(IProductRepository productRepository, StoreContext context)
        {
            _productRepository = productRepository;
            _context = context;
        }

        /// <summary>
        /// Ejecuta el commando para eliminacion de un producto
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<Unit> Handle(DeleteProductByIdCommand request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                // Recuperamos el producto con el id
                var removable = _productRepository.GetById(request.ProductId);
                // Si es nulo, lanzamos excepcion de ddatos
                if (removable is null)
                    throw new DataException("Product not found");
                // Lo eliminamos del repositorio
                _productRepository.Delete(removable);
                // Confirmamos los cambios
                _context.SaveChanges();
                // Devolvemos la unidad
                return new Unit();
            });
        }
    }
}
