﻿using ApiClientExample.Contracts;
using ApiClientExample.Data;
using ApiClientExample.Domain.Aggregates.ProductAggregate;
using ApiClientExample.Domain.Enumerations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.UpdateProduct
{
    public class UpdateProductHandler : ICommandHandler<UpdateProductCommand>
    {
        /// <summary>
        /// Acceso a la entidad de producto
        /// </summary>
        private readonly IProductRepository _productRepository;

        /// <summary>
        /// Unidad de trabajo para confirmar los cambios realizados
        /// </summary>
        private readonly StoreContext _context;

        /// <summary>
        /// Constructor del administrador del comando
        /// </summary>
        /// <param name="productRepository"></param>
        /// <param name="context"></param>
        public UpdateProductHandler(IProductRepository productRepository, StoreContext context)
        {
            _productRepository = productRepository;
            _context = context;
        }

        /// <summary>
        /// Ejecuta el commando de actualizacion de pproducto
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<Unit> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                var all = _productRepository.GetAll().ToList();
                // Obtenemos el producto a modificar
                var upgradeable = _productRepository.GetById(request.ProductId);
                // Si el objeto no existe salimos
                if (upgradeable is null)
                    throw new DataException("Object not found");
                // Creamos la nueva categoria
                var newCategory = ProductCategory.GetById(request.NewCategoryId);
                // Actualizamos la entidad
                upgradeable.UpdateProductInformation(request.NewName, newCategory, request.NewDescription, request.NewStock);
                // Almacenamos los cambios en el repositorio
                _productRepository.Update(upgradeable);
                // Salvamos los cambios
                _context.SaveChanges();
                // Devolvemmos vacio
                return new Unit();
            });
        }
    }
}
