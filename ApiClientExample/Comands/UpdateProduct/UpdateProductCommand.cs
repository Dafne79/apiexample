﻿using ApiClientExample.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.UpdateProduct
{
    public class UpdateProductCommand : ICommand
    {
        /// <summary>
        /// Id del comando de actualziacion
        /// </summary>
        public Guid Id { get; } = Guid.NewGuid();

        /// <summary>
        /// Id del producto a actualizar
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Nuevo nombre a actualizar
        /// </summary>
        public string NewName { get; set; }

        /// <summary>
        /// Id de la nueva categoria
        /// </summary>
        public int NewCategoryId { get; set; }

        /// <summary>
        /// Nueva descripcion
        /// </summary>
        public string NewDescription { get; set; }

        /// <summary>
        /// Nueva cantidad de stock
        /// </summary>
        public int NewStock { get; set; }

        
    }
}
