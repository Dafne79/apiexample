﻿using ApiClientExample.Contracts;
using ApiClientExample.Data;
using ApiClientExample.Domain.Aggregates.ProductAggregate;
using ApiClientExample.Domain.Enumerations;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.CreateProduct
{
    public class ProductCreationHandler : ICommandHandler<ProductCreationCommand,int>
    {
        /// <summary>
        /// Contexto de las entidades de base de datos
        /// </summary>
        private readonly IProductRepository _productRepository;

        /// <summary>
        /// Unidad de trabajo para la confirmacion de las transacciones
        /// </summary>
        private readonly StoreContext _context;

        public ProductCreationHandler(IProductRepository productRepository, StoreContext context)
        {
            _productRepository = productRepository;
            _context = context;
        }


        /// <summary>
        /// Ejecuta el comando que se recibe y efectua las operaciones en los agregados correspondientes
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<int> Handle(ProductCreationCommand request, CancellationToken cancellationToken)
        {
            // Lo hacemos una tarea
            return Task.Run(() =>
            {
                // Buscamos el catalogo de categoria
                var productCategory = ProductCategory.GetById(request.CategoryId) as ProductCategory; 
                // Creamos el producto
                var newProduct = new Product(request.Name, productCategory, request.Description, request.Stock);
                // Lo guardamos en el repositorio
                _productRepository.Add(newProduct);
                // Confirmamos los cambios
                _context.SaveChanges();
                // Devolvemos el id creado
                return newProduct.Id;
            });
            
        }
    }
}
