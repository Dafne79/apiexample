﻿using ApiClientExample.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Comands.CreateProduct
{
    /// <summary>
    /// Comando para crear un nuevo producto y devolviendo el id de creacion
    /// </summary>
    public class ProductCreationCommand : ICommand<int>
    {
        /// <summary>
        /// Id del commando para tareas de seguimmiento y logging
        /// </summary>
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Id de la categoria del producto
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Id del estatus del producto
        /// </summary>
        public int StatusId { get; set; }

        /// <summary>
        /// Cantidad de existencias
        /// </summary>
        public int Stock { get; set; }
    }
}
