using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MediatR;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiClientExample.Data;
using System.Data;
using ApiClientExample.Domain.Aggregates.ProductAggregate;
using ApiClientExample.Data.Repositories;

namespace ApiClientExample
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        private readonly string MyPolicies = "MyCors";

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Agregamos la fabrica de conexion
            services.AddScoped<IDbConnection>((x) => new SqliteConnection("DataSource=Data/Store.db"));
            // Agregamos la base de datos en memoria
            services.AddDbContext<StoreContext>();
            // Agregamos el repositorio
            services.AddScoped<IProductRepository, ProductRepository>();
            // Agregamos mediatR para habilitar CQRS
            services.AddMediatR(typeof(Startup));
            // Agregamos los controladores
            services.AddControllers();
            // Documentacion con swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ApiClientExample", Version = "v1" });
            });
            // Agregamos politicas de cors
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyPolicies, builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ApiClientExample v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(MyPolicies);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
