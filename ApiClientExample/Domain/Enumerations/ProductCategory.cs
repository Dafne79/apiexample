﻿using ApiClientExample.Domain.Aggregates.ProductAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ApiClientExample.Domain.Enumerations
{
    public class ProductCategory : EnumerationBase<ProductCategory>
    {
        public static ProductCategory Office = new ProductCategory(1,"Oficina",1,true);
        public static ProductCategory House = new ProductCategory(2,"Casa",1,true);
        public static ProductCategory Building = new ProductCategory(3,"Construccion",1,true);
        public static ProductCategory Food = new ProductCategory(4,"Alimentos",1,true);
        public static ProductCategory Sports = new ProductCategory(5,"Deportes",1,true);

        /// <summary>
        /// Constructor por parametros de la categoria de producto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="order"></param>
        /// <param name="isVisible"></param>
        public ProductCategory(int id, string name, int order, bool isVisible)
        {
            this.Id = id;
            this.Name = name;
            this.Order = order;
            this.IsVisible = isVisible;
        }

        /// <summary>
        /// Constructor sin parametros para EF
        /// </summary>
        public ProductCategory() { }
    }
}
