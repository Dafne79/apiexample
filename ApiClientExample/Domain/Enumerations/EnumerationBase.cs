﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ApiClientExample.Domain.Enumerations
{
    public abstract class EnumerationBase<TEnumeration>
        where TEnumeration : EnumerationBase<TEnumeration>
    {

        /// <summary>
        /// Identificador de la categoria de producto
        /// </summary>
        public int Id { get; protected set; }

        /// <summary>
        /// Nombre de la categoria
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Indica el orden de presentacion
        /// </summary>
        public int Order { get; protected set; }

        /// <summary>
        /// Indica si el elemento del catalogo es visible
        /// </summary>
        public bool IsVisible { get; protected set; }


        /// <summary>
        /// Recupera una categoria de producto en base a su id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static TEnumeration GetById(int id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id) as TEnumeration;
        }

        /// <summary>
        /// Recupera todas las categorias de producto declaradas dentro de
        /// esta clase usando reflexion
        /// </summary>
        /// <returns></returns>
        public static TEnumeration[] GetAll()
        {
            var enumerationType = typeof(TEnumeration);
            return enumerationType.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly)
                .Where(info => enumerationType.IsAssignableFrom(info.FieldType))
                .Select(info => info.GetValue(null))
                .Cast<TEnumeration>()
                .ToArray();
        }
    }
}
