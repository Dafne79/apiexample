﻿using ApiClientExample.Domain.Aggregates.ProductAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ApiClientExample.Domain.Enumerations
{
    public class ProductStatus : EnumerationBase<ProductStatus>
    {
        public static ProductStatus AvailableStock = new ProductStatus(1, "Existencia disponible", 1, true);
        public static ProductStatus OutOfStock = new ProductStatus(2, "Fuera de existencia", 1, true);

        /// <summary>
        /// Constructor por parametros del status de producto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="order"></param>
        /// <param name="isVisible"></param>
        public ProductStatus(int id, string name, int order, bool isVisible)
        {
            this.Id = id;
            this.Name = name;
            this.Order = order;
            this.IsVisible = isVisible;
        }

        /// <summary>
        /// Constructor sin parametros para EF
        /// </summary>
        public ProductStatus() { }
    }
}
