﻿using ApiClientExample.Domain.Enumerations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Domain.Aggregates.ProductAggregate
{
    public class Product
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Categoria del producto
        /// </summary>
        public ProductCategory Category { get; set; }

        /// <summary>
        /// Descripcion larga del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// status del producto
        /// </summary>
        public ProductStatus Status { get; set; }

        /// <summary>
        /// Cantidad del producto
        /// </summary>
        public int Stock { get; set; }

        public Product()
        {

        }

        public Product(string name, ProductCategory category, string description, int stock)
        {
            Product.Validate(name,category,description,stock);
            this.Id = 0;
            this.Name = name;
            this.Category = category;
            this.Description = description;
            this.Stock = stock;
            this.Status = CalculateStatus(this.Stock);
        }

        /// <summary>
        /// Aplicamos validaciones de construccion
        /// </summary>
        /// <param name="name"></param>
        /// <param name="category"></param>
        /// <param name="description"></param>
        /// <param name="stock"></param>
        private static void Validate(string name, ProductCategory category, string description, int stock)
        {
            // Lista de errores
            List<string> errors = new List<string>();
            // Validaciones de entidad
            if (string.IsNullOrEmpty(name)) 
                errors.Add("Product must have name");
            if (category is null)
                errors.Add("Product must have category");
            if (string.IsNullOrEmpty(description))
                errors.Add("Product must have description");
            if (stock < 0)
                errors.Add("Stock product can not be negative");
            // Si no hay errores salimos
            if (!errors.Any()) return;
            // Unimos todos los errores
            string jsonErrors = JsonConvert.SerializeObject(errors);
            // Creamos una excepcion 
            var validationException = new AggregateException("Product cannot be built");
            // Agregamos los detalles
            validationException.Data.Add("DetailErrors",jsonErrors);
            // Lanzamos la excepcion
            throw validationException;
        }

        /// <summary>
        /// Calcula el status futuro en base al stock que se pasa y al estado actual
        /// </summary>
        /// <param name="stock"></param>
        /// <returns></returns>
        private ProductStatus CalculateStatus(int stock)
        {
            // Si habia productos y el stock es cero, el status cambia a fuera de stock
            if ((this.Status is null || this.Status.Id == ProductStatus.AvailableStock.Id) && stock <= 0)
                return ProductStatus.OutOfStock;
            // Si no habia productos y el stock es mayor a cero, el status cambia a disponible
            if ((this.Status is null || this.Status.Id == ProductStatus.OutOfStock.Id) && stock > 0)
                return ProductStatus.AvailableStock;
            // De cualquier otra forma, se devuelve el mismo status
            return this.Status;
        }

        /// <summary>
        /// Actualizammos el stock y calculamos el nuevo status del elemento
        /// </summary>
        /// <param name="stock"></param>
        private void UpdateStock(int stock)
        {
            this.Stock = stock;
            this.Status = CalculateStatus(this.Stock);
        }

        /// <summary>
        /// Actualizamosla informacion del producto si los parametros estan dentro de los valores aceptables
        /// </summary>
        /// <param name="name"></param>
        /// <param name="category"></param>
        /// <param name="description"></param>
        /// <param name="stock"></param>
        public void UpdateProductInformation(string name, ProductCategory category, string description, int stock)
        {
            if (!string.IsNullOrEmpty(name)) 
                this.Name = name;
            if (!(category is null)) 
                this.Category = category;
            if (!string.IsNullOrEmpty(description)) 
                this.Description = description;
            if (stock >= 0) UpdateStock(stock);
        }
    }
}
