﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiClientExample.Domain.Aggregates.ProductAggregate
{
    public interface IProductRepository
    {
        /// <summary>
        /// Obtiene todos los productos desde la base de datos
        /// </summary>
        /// <returns></returns>
        IEnumerable<Product> GetAll();

        /// <summary>
        /// Obtiene un producto por id
        /// </summary>
        /// <returns></returns>
        Product GetById(int id);

        /// <summary>
        /// Agrega un nuevo producto a la lista de productos
        /// </summary>
        /// <param name="product"></param>
        void Add(Product product);

        /// <summary>
        /// Actualiza un producto especifico
        /// </summary>
        /// <param name="product"></param>
        void Update(Product product);

        /// <summary>
        /// Elimina un producto con el id especificado
        /// </summary>
        /// <param name="id"></param>
        void Delete(Product product);
    }
}
